# RandomTaskPicker

Randomly selects up to 9 tasks from a user defined list.
(I'm assuming that you're working on each task for up to an hour and don't want to work for 12 hours a day.)
You can always pick more if you want.

Each task has a weighted priority; the greater priority the more likely it is to be picked.

If tasks are NOT _picked_ then their priority is increased.
If tasks ARE _picked_ then their priority is reset to their defaults.

If a task is _previewed_ its priority is not changed.


## Live Page

[https://carlbateman.gitlab.io/random-task-picker/](https://carlbateman.gitlab.io/random-task-picker/)
