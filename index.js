﻿function filterInt(value) {
	value = Math.floor(value);
	value = value > 9 ? 9 : value;
	value = value < 1 ? 1 : value;
	return value;
}

// Open a database
let request = indexedDB.open("todoList", 1);

let numOfTasks = sessionStorage.getItem("numOfTasks") || 1;
let viewState = sessionStorage.getItem("viewState") || "admin";
let sortByNameState = sessionStorage.getItem("sortByName") || "false";


// Create the schema
request.onupgradeneeded = function (event) {
	let db = event.target.result;
	let objectStore = db.createObjectStore("tasks", { keyPath: "taskId" });
	objectStore.createIndex("taskName", "taskName", { unique: false });
	objectStore.createIndex("completed", "completed", { unique: false });
	objectStore.createIndex("priority", "priority", { unique: false });
	objectStore.createIndex("default_priority", "default_priority", { unique: false });
};

// Success handler for opening the database
request.onsuccess = function (event) {
	let txtnumOfTasks = document.getElementById("numOfTasks");
	txtnumOfTasks.value = numOfTasks;
	txtnumOfTasks.addEventListener("input", function () {
		numOfTasks = document.getElementById("numOfTasks").value;
		sessionStorage.setItem("numOfTasks", numOfTasks);
	});

	let sortByName = document.getElementById("sortByName");
	document.getElementById("sortByName").checked = (sortByNameState === 'true');

	sortByName.addEventListener("click", function (e) {
		sortByNameState = e.srcElement.checked;
		sessionStorage.setItem("sortByName", sortByNameState);
		getTasks(displayAdmin);

	});

	let admin = document.getElementById("btnAdmin");
	admin.addEventListener("click", function () {
		document.getElementById('admin').style.display = 'block';
		document.getElementById('select-task').style.display = 'none';
		viewState = "admin";
		sessionStorage.setItem("viewState", viewState);
	});

	let selectTask = document.getElementById("btnSelect");
	selectTask.addEventListener("click", function () {
		document.getElementById('admin').style.display = 'none';
		document.getElementById('select-task').style.display = 'block';
		viewState = "select";
		sessionStorage.setItem("viewState", viewState);
	});

	let setOfTasks;
	let db = event.target.result;

	if (viewState == "admin") {
		document.getElementById('select-task').style.display = 'none';
	} else {
		document.getElementById('admin').style.display = 'none';
		}

	// Add a task
	function addTask(taskName, taskPriority, taskDefaultPriority) {
		let transaction = db.transaction(["tasks"], "readwrite");
		let objectStore = transaction.objectStore("tasks");
		let task = { taskId: Date.now(), taskName: taskName, completed: false, priority: taskPriority, default_priority: taskDefaultPriority };
		let request = objectStore.add(task);

		request.onsuccess = function (event) {
			console.log("Task added to the database");
			displayTasks();
		};

		request.onerror = function (event) {
			console.log("Error adding task to the database");
		};
	}

	// Get all tasks
	function getTasks(cb) {
		let transaction = db.transaction(["tasks"], "readonly");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.getAll();

		request.onsuccess = function (event) {
			cb(event.target.result);
		};

		request.onerror = function (event) {
			console.log("Error getting tasks from the database");
		};
	}

	// Mark a task as completed
	function markCompleted(taskId) {
		let transaction = db.transaction(["tasks"], "readwrite");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.get(taskId);

		request.onsuccess = function (event) {
			let task = event.target.result;
			task.completed = !task.completed;
			objectStore.put(task);
			displayTasks();
		};
		request.onerror = function (event) {
			console.log("Error marking task as completed");
		};
	}

	function updateTaskName(taskId, taskName) {
		let transaction = db.transaction(["tasks"], "readwrite");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.get(taskId);

		request.onsuccess = function (event) {
			let task = event.target.result;
			task.taskName = taskName;
			objectStore.put(task);
			displayTasks();
		};
		request.onerror = function (event) {
			console.log("Error updating task priority");
		};
	}

	function updatePriority(taskId, priority) {
		let transaction = db.transaction(["tasks"], "readwrite");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.get(taskId);

		request.onsuccess = function (event) {
			let task = event.target.result;
			task.priority = priority;
			objectStore.put(task);
			displayTasks();
		};
		request.onerror = function (event) {
			console.log("Error updating task priority");
		};
	}

	function updateDefaultPriority(taskId, default_priority) {
		let transaction = db.transaction(["tasks"], "readwrite");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.get(taskId);

		request.onsuccess = function (event) {
			let task = event.target.result;
			task.default_priority = default_priority;
			objectStore.put(task);
			displayTasks();
		};
		request.onerror = function (event) {
			console.log("Error updating task default priority");
		};
	}

	// Delete a task
	function deleteTask(taskId) {
		let transaction = db.transaction(["tasks"], "readwrite");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.delete(taskId);

		request.onsuccess = function (event) {
			console.log("Task deleted from the database");
			displayTasks();
		};

		request.onerror = function (event) {
			console.log("Error deleting task from the database");
		};
	}

	// Display all tasks
	function createInputElement(type, value, min, max) {
		let input = document.createElement("input");
		input.type = type;
		input.value = value;
		input.min = min;
		input.max = max;
		return input;
	}

	function displayTasks() {
		getTasks(displayAdmin);
	}

	function displayAdmin(tasks) {
		let tasksElement = document.getElementById("tasks-admin");
		tasksElement.innerHTML = "";

		if (sortByNameState) {
			tasks.sort(function (a, b) {
				let x = a.taskName.toLowerCase();
				let y = b.taskName.toLowerCase();
				if (x < y) { return -1; }
				if (x > y) { return 1; }
				return 0;
			});
		} else {
			tasks.sort(function (a, b) { return a.taskId - b.taskId; });
		}

		tasks.forEach(function (task) {
			let row = document.createElement("tr");
			let taskNameCell = document.createElement("td");
			row.appendChild(taskNameCell);

			let taskNameInput = document.createElement("input");
			taskNameInput.type = "text";
			//taskNameInput.disabled = true;
			taskNameInput.value = task.taskName;
			taskNameCell.appendChild(taskNameInput);
			taskNameInput.addEventListener("blur", function () {
				updateTaskName(task.taskId, taskNameInput.value);
			});

			let priorityCell = document.createElement("td");
			let txtPriority = createInputElement("number", task.priority, 1, 9);

			txtPriority.addEventListener("input", function () {
				updatePriority(task.taskId, filterInt(txtPriority.value));
			});

			priorityCell.appendChild(txtPriority);
			row.appendChild(priorityCell);


			let defaultPriorityCell = document.createElement("td");
			let txtDefaultPriority = createInputElement("number", task.default_priority, 1, 9);

			txtDefaultPriority.addEventListener("input", function () {
				updateDefaultPriority(task.taskId, filterInt(txtDefaultPriority.value));
			});

			defaultPriorityCell.appendChild(txtDefaultPriority);
			row.appendChild(defaultPriorityCell);

			let completedCell = document.createElement("td");
			let checkbox = document.createElement("input");
			checkbox.type = "checkbox";
			checkbox.checked = task.completed;
			checkbox.addEventListener("click", function () {
				markCompleted(task.taskId);
			});
			completedCell.appendChild(checkbox);
			row.appendChild(completedCell);

			let actionsCell = document.createElement("td");
			let deleteButton = document.createElement("button");
			deleteButton.innerHTML = "Delete";
			deleteButton.addEventListener("click", function () {
				deleteTask(task.taskId);
			});
			actionsCell.appendChild(deleteButton);
			row.appendChild(actionsCell);

			tasksElement.appendChild(row);
		});

		let row = document.createElement("tr");
		row.innerHTML = `
				<td><input id="taskName" type="text" /></td>
				<td><input id="taskPriority" type="number" min="1" max="9" value="1" oninput="event.target.value = filterInt(event.target.value)" /></td>
				<td><input id="taskDefaultPriority" type="number" min="1" max="9" value="1" oninput="event.target.value = filterInt(event.target.value)" /></td>
				<td><input type="checkbox"/></td>
				<td><button type="button" id="addTaskButton">Add</button></td>`;

		tasksElement.appendChild(row);

		// Add task button click event
		let addTaskButton = document.getElementById("addTaskButton");
		addTaskButton.addEventListener("click", function () {
			let taskName = document.getElementById("taskName").value;

			if (taskName.trim() === "") return;

			let taskPriority = document.getElementById("taskPriority").value;
			let taskDefaultPriority = document.getElementById("taskDefaultPriority").value;
			addTask(taskName, taskPriority, taskDefaultPriority);
		});

	}

	// Delete completed tasks button click event
	function deleteTasks(tasks) {
		tasks.forEach(function (task) {
			if (task.completed) {
				deleteTask(task.taskId);
			}
		});
	}

	let deleteCompletedButton = document.getElementById("deleteCompletedButton");
	deleteCompletedButton.addEventListener("click", function () {
		let tasks = getTasks(deleteTasks);
	});

	let getRandomTasksButton = document.getElementById("getRandomTasks");
	getRandomTasksButton.addEventListener("click", function () {
		//let numOfTasks = document.getElementById("numOfTasks").value;

		let tasks = getRandomTasks(numOfTasks);
	});

	let previewTasksButton = document.getElementById("previewTasks");
	previewTasksButton.addEventListener("click", function () {
		//let numOfTasks = document.getElementById("numOfTasks").value;

		let tasks = previewTasks(numOfTasks);
	});

	let updatePrioritiesButton = document.getElementById("updatePriorities");
	updatePrioritiesButton.addEventListener("click", function () {
		//let numOfTasks = document.getElementById("numOfTasks").value;

		let tasks = updatePriorities(numOfTasks);
	});

	let resetPrioritiesButton = document.getElementById("resetPriorities");
	resetPrioritiesButton.addEventListener("click", function () {
		//let numOfTasks = document.getElementById("numOfTasks").value;

		let tasks = resetPriorities(numOfTasks);
	});

	// Display tasks when the page loads
	displayTasks();

	function updatePriorities() {
		if (setOfTasks.size == 0) {
			return;
		}

		let transaction = db.transaction(["tasks"], "readonly");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.getAll();

		request.onsuccess = function (event) {
			let tasks = event.target.result;

			tasks.forEach(function (task) {
				if (task.completed == false) {
					if (setOfTasks.has(task.taskName)) {
						updatePriority(task.taskId, task.default_priority);
					} else {
						updatePriority(task.taskId, parseInt(task.priority) + 1);
					}
				}
			});

			setOfTasks.clear();
		}
	}

	function resetPriorities() {
		let transaction = db.transaction(["tasks"], "readonly");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.getAll();

		request.onsuccess = function (event) {
			let tasks = event.target.result;

			tasks.forEach(function (task) {
				updatePriority(task.taskId, task.default_priority);
			});

			setOfTasks.clear();
		}
	}

	function getRandomTasks(count) {
		let tasksElement = document.getElementById("tasks-select");
		tasksElement.innerHTML = "";

		let transaction = db.transaction(["tasks"], "readonly");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.getAll();

		request.onsuccess = function (event) {
			let randomTasks = [];
			let tasks = event.target.result;

			// add instances of name basd on priority, higher priority add more
			tasks.forEach(function (task) {
				if (task.completed == false) {
					let a = [...new Array(task.priority)].map(() => task.taskName);
					randomTasks.push(...a);
				}
			});

			// shuffle
			for (let i = 0; i < randomTasks.length; i++) {
				let n = Math.floor(Math.random() * randomTasks.length);
				let tmpTask = randomTasks[n];
				randomTasks[n] = randomTasks[i];
				randomTasks[i] = tmpTask;
			}

			// create set to remove duplicates
			let setOfTasks = new Set(randomTasks);
			randomTasks = [...setOfTasks];

			// return first n
			let numOfTasks = document.getElementById("numOfTasks").value;
			numOfTasks = numOfTasks > randomTasks.length ? randomTasks.length : numOfTasks;

			randomTasks = randomTasks.slice(0, numOfTasks);
			setOfTasks = new Set(randomTasks);

			for (let i = 0; i < numOfTasks; i++) {
				let item = document.createElement("li");
				item.innerHTML = randomTasks[i];

				tasksElement.appendChild(item);
			}

			// update the priority weighting
			tasks.forEach(function (task) {
				if (task.completed == false) {
					if (setOfTasks.has(task.taskName)) {
						updatePriority(task.taskId, task.default_priority);
					} else {
						updatePriority(task.taskId, parseInt(task.priority) + 1);
					}
				}
			});
		};

		request.onerror = function (event) {
			console.log("Error getting tasks from the database");
		};
	}

	function previewTasks(count) {
		let tasksElement = document.getElementById("tasks-select");
		tasksElement.innerHTML = "";

		let transaction = db.transaction(["tasks"], "readonly");
		let objectStore = transaction.objectStore("tasks");
		let request = objectStore.getAll();

		request.onsuccess = function (event) {
			let randomTasks = [];
			let tasks = event.target.result;

			// add instances of name basd on priority, higher priority add more
			tasks.forEach(function (task) {
				if (task.completed == false) {
					let a = [...new Array(task.priority)].map(() => task.taskName);
					randomTasks.push(...a);
				}
			});

			// shuffle
			for (let i = 0; i < randomTasks.length; i++) {
				let n = Math.floor(Math.random() * randomTasks.length);
				let tmpTask = randomTasks[n];
				randomTasks[n] = randomTasks[i];
				randomTasks[i] = tmpTask;
			}

			// create set to remove duplicates
			setOfTasks = new Set(randomTasks);
			randomTasks = [...setOfTasks];

			// return first n
			//let numOfTasks = document.getElementById("numOfTasks").value;
			numOfTasks = numOfTasks > randomTasks.length ? randomTasks.length : numOfTasks;

			randomTasks = randomTasks.slice(0, numOfTasks);
			setOfTasks = new Set(randomTasks);

			for (let i = 0; i < numOfTasks; i++) {
				let item = document.createElement("li");
				item.innerHTML = randomTasks[i];

				tasksElement.appendChild(item);
			}
		};




		request.onerror = function (event) {
			console.log("Error getting tasks from the database");
		};


	}


};