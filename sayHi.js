﻿function sayHi(name = 'there') {
	return `Hi ${name}!`;
}

export { sayHi };