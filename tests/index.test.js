﻿//These tests use Jest to test the functionality of the IndexedDB to -do list code.The`beforeAll` function is used to open a new database for testing and the`afterAll` function is used to close and delete the test database.The `describe` and `it` functions are used to organize and run the tests.Each test uses the IndexedDB API to interact with the test database and Jest's `expect` function to check if the expected outcome is achieved.

//It's important to note that these tests are just a basic example, you may want to add more test cases to ensure the full coverage of your application.


const indexedDB = window.indexedDB;

describe("IndexedDB To-Do List", () => {
  let db;

  beforeAll(async () => {
    const request = indexedDB.open("todoListTest", 1);

    request.onupgradeneeded = function (event) {
      db = event.target.result;
      db.createObjectStore("tasks", { keyPath: "taskId" });
    };

    request.onsuccess = function (event) {
      db = event.target.result;
    };

    await new Promise(resolve => request.onsuccess = resolve);
  });

  afterAll(() => {
    db.close();
    indexedDB.deleteDatabase("todoListTest");
  });

  describe("addTask()", () => {
    it("should add a task to the database", async () => {
      const transaction = db.transaction(["tasks"], "readwrite");
      const objectStore = transaction.objectStore("tasks");

      addTask("Take out the trash", objectStore);
      await new Promise(resolve => transaction.oncomplete = resolve);

      const request = objectStore.get(1);
      let task;
      request.onsuccess = function (event) {
        task = event.target.result;
      };

      await new Promise(resolve => request.onsuccess = resolve);
      expect(task.taskName).toBe("Take out the trash");
    });
  });

  describe("getTasks()", () => {
    it("should return all tasks from the database", async () => {
      const transaction = db.transaction(["tasks"], "readwrite");
      const objectStore = transaction.objectStore("tasks");

      addTask("Do laundry", objectStore);
      addTask("Wash dishes", objectStore);
      await new Promise(resolve => transaction.oncomplete = resolve);

      const tasks = getTasks(objectStore);
      await new Promise(resolve => transaction.oncomplete = resolve);

      expect(tasks.length).toBe(3);
      expect(tasks[0].taskName).toBe("Take out the trash");
      expect(tasks[1].taskName).toBe("Do laundry");
      expect(tasks[2].taskName).toBe("Wash dishes");
    });
  });

  describe("markCompleted()", () => {
    it("should mark a task as completed in the database", async () => {
      const transaction = db.transaction(["tasks"], "readwrite");
      const objectStore = transaction.objectStore("tasks");

      markCompleted(1, objectStore);
      await new Promise(resolve => transaction.oncomplete = resolve);

      const request = objectStore.get(1);
      let task;
      request.onsuccess = function (event) {
        task = event.target.result;
      };

      await new Promise(resolve => request.onsuccess = resolve);
      expect(task.completed).toBe(true);
    });
  });

  describe("deleteTask()", () => {

    it("should delete a task from the database", async () => {
      const transaction = db.transaction(["tasks"], "readwrite");
      const objectStore = transaction.objectStore("tasks");
    });
  });